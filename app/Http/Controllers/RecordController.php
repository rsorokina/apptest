<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Record;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("records");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     * Search Records
     *
     */

    public function searchRecords(Request $request)
    {
        $queryBuilder = Record::query();
        if ($request->has("name") && $request->input("name") !=  null)
            $queryBuilder->where("name", "like", "%".$request->input("name").'%');

        if ($request->has("price_from") && $request->input("price_from") !=  null)
            $queryBuilder->where("price", ">=", floatval($request->input("price_from")));

        if ($request->has("price_to") && $request->input("price_to") !=  null)
            $queryBuilder->where("price", "<=", floatval($request->input("price_to")));


        foreach (["bedrooms", "bathrooms", "storeys", "garages"] as $param) {
            if ($request->has($param) && $request->input($param) !=  null)
                $queryBuilder->where($param, "=", $request->input($param));
        }
        return response()->json($queryBuilder->get());
    }
}
