<?php

use Illuminate\Database\Seeder;
use App\Models\Record;

class RecordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = public_path().'/csv/property-data.csv';
        $delimiter = ",";
        $header = NULL;

        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = array_map('strtolower', $row);
                else {
                    Record::create(array_combine($header, $row));
                }

            }
            fclose($handle);
        }

    }
}
